DaybreakProfilePriest30300Conf = {
	{spell = 'Power Word: Shield', cell = {0, 0},},
	{spell = 'Weakened Soul', filter = 'HARMFUL', cell = {1, 0},},
	{spell = 'Renew', cell = {2, 0},},
	{spell = 'Prayer of Mending', cell = {3, 0},},
	{spell = 'Surge of Light', cell = {0, 1},},
	{spell = 'Serendipity', cell = {1, 1},},
	{spell = 'Borrowed Time', cell = {1, 1},},
	{spell = 'Pain Suppression', cell = {0, 2},},
	{spell = 'Guardian Spirit', cell = {1, 2},},
	{spell = 'Power Infusion', cell = {2, 2},},
	{spell = 'Inner Focus', cell = {3, 2},},
	{spell = 'Fear Ward', cell = {4, 2},},
}
