DaybreakProfileDeathknight30300Conf = {
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Ebon Plague', cell = {0, 0},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Blood Plague', cell = {1, 0},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Frost Fever', cell = {2, 0},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Unholy Blight', cell = {3, 0},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Gnaw', cell = {1, 1},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Strangulate', cell = {2, 1},},
	{filter = 'HARMFUL PLAYER', unit = 'target', spell = 'Chains of Ice', cell = {0, 1},},
	{unit = 'player', spell = 'Bone Shield', cell = {0, 2},},
	{unit = 'player', spell = 'Icebound Fortitude', cell = {1, 2},},
	{unit = 'player', spell = 'Anti-Magic Shell', cell = {2, 2},},
	{unit = 'player', spell = 'Anti-Magic Zone', cell = {3, 2},},
}
