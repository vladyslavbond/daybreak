DaybreakProfileWarrior40300Conf = {
	--[[ Major ]]--
	--[[ Defensive ]]--
	{spell = 'Shield Wall', cell = {0, 0}},
	{spell = 'Spell Block', cell = {0, 1}},
	{spell = 'Shield Block', cell = {0, 1}},
	{spell = 'Last Stand', cell = {1, 0}},
	--[[ Offensive ]]--
	{spell = 'Recklessness', cell = {3, 2}},
	{spell = 'Retaliation', cell = {2, 2}},
	{spell = 'Bladestorm', cell = {1, 2}},
	--[[ Minor ]]--
	--[[ Defensive ]]--
	{spell = 'Spell Reflection', cell = {0, 2}},
	{spell = 'Glyph of Blocking', cell = {0, 4}},
	--[[ Offensive ]]--
	{filter = 'HARMFUL', unit = 'target', spell = 'Sunder Armor', cell = {1, 3},},
	{spell = 'Victorious', cell = {1, 3}},
	{spell = 'Battle Trance', cell = {2, 3}},
	{spell = 'Sword and Board', cell = {0, 3}},
	{spell = 'Sudden Death', cell = {0, 3}},
	{spell = 'Incite', cell = {3, 3}},
	--[[ Other ]]--
	{spell = 'Hand of Protection', cell = {2, 0}},
	{spell = 'Hand of Freedom', cell = {3, 0}},
	{spell = 'Pain Suppression', cell = {4, 0}},
	{spell = 'Guardian Spirit', cell = {4, 0}},
}

DaybreakProfileWarrior30300Conf = DaybreakProfileWarrior40300Conf
