DaybreakProfilePaladin40300Conf = {
	{spell = 'Ardent Defender', cell = {2, 2}},
	{spell = 'Divine Protection', cell = {3, 2}},
	{spell = 'Divine Sacrifice', cell = {4, 2}},
	{spell = 'Divine Shield', cell = {0, 2}},
	{spell = 'Hand of Protection', cell = {0, 2}},

	{spell = 'Sacred Duty', cell = {0, 0}},
	{spell = 'Grand Crusader', cell = {1, 0}},
	{spell = 'Guarded by the Light', cell = {2, 0}},
	{spell = 'Holy Shield', cell = {3, 0}},

	{spell = 'Divine Purpose', cell = {0, 0}},
	{spell = 'The Art of War', cell = {1, 0}},
	{spell = 'Long arm of the Law', cell = {2, 0}},
	{spell = 'Zealotry', cell = {0, 2}},
	{spell = 'Inquisition', cell = {1, 1}},
	{spell = 'Divine Plea', cell = {0, 1}},

	{spell = 'Infusion of Light', cell = {0, 0}},
	{spell = 'Daybreak', cell = {1, 0}},
	{spell = 'Divine Favor', cell = {2, 0}},
	{spell = 'Avenging Wrath', cell = {3, 0}},
}
