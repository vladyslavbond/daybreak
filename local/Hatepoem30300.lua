DaybreakProfilePaladin30300Conf = {
	--[[ Assored buffs ]]--
	'Judgements of the Pure',
	'Infusion of Light',
	"Light's Grace",
	'Divine Plea',
	'Divine Favor',
	'Divine Shield',
	'Avenging Wrath',
	'Divine Protection',
	'Divine Illumination',
	'Divine Sacrifice',

	--[[ Player's own currently active aura. ]]--
	{spell = 'Devotion Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Retribution Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Concentration Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Shadow Resistance Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Frost Resistance Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Fire Resistance Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Crusader Aura', cell = {0, 4}, filter = 'PLAYER HELPFUL'},
	{spell = 'Aura Mastery', cell = {1, 4}, filter = 'PLAYER HELPFUL'},

	--[[ Aura effects, including from other party members. ]]--
	--{spell = 'Devotion Aura', cell = {0, 5}, filter = 'HELPFUL'},
	--{spell = 'Retribution Aura', cell = {1, 5}, filter = 'HELPFUL'},
	--{spell = 'Concentration Aura', cell = {2, 5}, filter = 'HELPFUL'},
	--{spell = 'Shadow Resistance Aura', cell = {3, 5}, filter = 'HELPFUL'},
	--{spell = 'Frost Resistance Aura', cell = {4, 5}, filter = 'HELPFUL'},
	--{spell = 'Fire Resistance Aura', cell = {5, 5}, filter = 'HELPFUL'},
	--{spell = 'Crusader Aura', cell = {6, 5}, filter = 'HELPFUL'},

	'Hand of Freedom',
	'Hand of Protection',
	'Hand of Salvation',
	'Hand of Sacrifice',

	--[[ Blessings ]]--
	{spell = 'Blessing of Kings', cell = {0, 3},},
	{spell = 'Blessing of Wisdom', cell = {1, 3},},
	{spell = 'Blessing of Might', cell = {2, 3},},
	{spell = 'Blessing of Sanctuary', cell = {3, 3},},
	{spell = 'Greater Blessing of Kings', cell = {0, 3},},
	{spell = 'Greater Blessing of Wisdom', cell = {1, 3},},
	{spell = 'Greater Blessing of Might', cell = {2, 3},},
	{spell = 'Greater Blessing of Sanctuary', cell = {3, 3},},
}
