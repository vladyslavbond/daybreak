local function init(rootFrame)
	assert(rootFrame ~= nil)

	rootFrame:UnregisterAllEvents()
	rootFrame:SetScript('OnEvent', nil)

	local _, classDesignation = UnitClass('player')
	assert(classDesignation ~= nil)
	assert('string' == type(classDesignation))
	classDesignation = string.upper(strtrim(classDesignation))

	local className = string.upper(string.sub(classDesignation, 1, 1))
	className = className .. string.lower(string.sub(classDesignation, 2, string.len(classDesignation)))

	local _, _, _, interfaceNumber = GetBuildInfo()
	assert(interfaceNumber ~= nil)
	assert('number' == type(interfaceNumber))
	interfaceNumber = math.abs(interfaceNumber)

	local n = string.format('DaybreakProfile%s%dFrame', className, interfaceNumber)
	local profileFrame = CreateFrame('FRAME', n, rootFrame, 'DaybreakProfileTemplate')
	profileFrame:SetAttribute('class', classDesignation)
	profileFrame:SetAttribute('interface', interfaceNumber)
	profileFrame:SetSize(30 * 6, 30 * 4)
	profileFrame:SetPoint('BOTTOMLEFT', 0, 0)
end

function Daybreak.main(rootFrame)
	assert(rootFrame ~= nil)

	init(rootFrame)
	--rootFrame:RegisterEvent('PLAYER_LOGIN')
	--rootFrame:SetScript('OnEvent', init)
end
