Daybreak = {}

function Daybreak.trace(...)
	print(date('%X'), '[Daybreak]:', ...)
end

do
	if 'en' ~= string.lower(string.sub(GetLocale(), 1, 2)) then
		Daybreak.trace('warning: the default configuration only works for English game clients')
	end

	--[[ Given Cataclysm client, hide native spell proc indicators, that
	flash in the middle of the screen ]]--

	local _, _, _, interfaceNumber = GetBuildInfo()
	if interfaceNumber >= 40000 then
		SetCVar("displaySpellActivationOverlays", false)
	end
end
