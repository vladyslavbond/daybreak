--[[ WoW API ]]--
local UnitIsUnit = UnitIsUnit

--[[ See FrameXML ]]--
local DebuffTypeColor = DebuffTypeColor
local GameTooltip = GameTooltip

--[[ See FrameXML/SecureTemplates.lua ]]--
local SecureButton_GetUnit = SecureButton_GetUnit
local SecureButton_GetAttribute = SecureButton_GetAttribute

DaybreakAuraTemplate = {}

function DaybreakAuraTemplate.auraButtonGameTooltipShow(self)
	GameTooltip:SetOwner(self, "ANCHOR_BOTTOMLEFT");
	GameTooltip:SetFrameLevel(self:GetFrameLevel() + 2);
	local unitDesignation = SecureButton_GetUnit(self) or 'none'
	local filter = SecureButton_GetAttribute(self, 'filter') or self.filter
	local index = self.index
	local spellName = SecureButton_GetAttribute(self, 'spell') or self.spell
	if index then
		GameTooltip:SetUnitAura(unitDesignation, index, filter)
	elseif spellName then
		local rank = nil
		GameTooltip:SetUnitAura(unitDesignation, spellName, rank, filter)
	end
end

function DaybreakAuraTemplate.auraButtonGameTooltipHide()
	GameTooltip:Hide();
end

local function getAuraCategoryColor(auraCategory)
	if not auraCategory then
		--[[ Empty string is equivalent to 'none' by default for DebuffTypeColor. ]]--
		auraCategory = ''
	end

	local r = 1
	local g = 1
	local b = 1
	if auraCategory then
		assert(auraCategory ~= nil)
		assert('string' == type(auraCategory))
		auraCategory = strtrim(auraCategory)
		--[[ Empty string is permissible ]]--
		assert(string.len(auraCategory) >= 0)
		assert(string.len(auraCategory) <= 256)

		local colorTuple = DebuffTypeColor[auraCategory]
		r = colorTuple.r
		g = colorTuple.g
		b = colorTuple.b
	end

	return r, g, b
end

local function formatDuration(durationSec)
	assert(durationSec ~= nil)
	assert('number' == type(durationSec))

	--[[ Allow for negative durations ]]--
	local durationSecAbs = math.abs(durationSec)

	local t
	if durationSecAbs < 60 then
		t = string.format("%.0f", durationSec)
	elseif durationSecAbs < 60 * 60 then
		t = string.format("%.0f m", durationSec / 60)
	elseif durationSecAbs < 60 * 60 * 24 then
		t = string.format("%.0f h", durationSec / 60 / 60)
	else
		t = string.format("%.0f d", durationSec / 60 / 60 / 24)
	end
	return t
end

local function applyUnitAura(self, auraName, rank, pictureFile, chargeQuantity,
	auraCategory, durationSec, expirationInstance, ownerDesignation, empty,
	anotherMagicNumber, auraId, ...)

	local frameName = self:GetName()
	assert(frameName ~= nil)

	--[[ Artwork that is icon. ]]--

	local artwork = self.artwork or _G[string.format('%sArtwork', frameName)]
	assert(artwork ~= nil)
	if not self.artwork then
		self.artwork = artwork
	end

	if not pictureFile then
		pictureFile = 'Interface\\Icons\\INV_Misc_QuestionMark'
	end
	artwork:SetTexture(pictureFile)

	--[[ Text that is the quantity of charges remaining. ]]--

	local label2 = self.label2 or _G[string.format('%sText2', frameName)]
	assert(label2 ~= nil)
	if not self.label2 then
		self.label2 = label2
	end

	local t2 = nil

	if chargeQuantity then
		assert(chargeQuantity ~= nil)
		assert('number' == type(chargeQuantity))
		chargeQuantity = math.abs(math.floor(chargeQuantity))
		if chargeQuantity < 2 then
			t2 = nil
		elseif chargeQuantity < 100 then
			t2 = string.format('%d', chargeQuantity)
		else
			t2 = '>99'
		end
	end
	label2:SetText(t2)

	--[[ Overlay that is border. ]]--

	local overlay = self.overlay or _G[string.format('%sOverlay', frameName)]
	assert(overlay ~= nil)
	if not self.overlay then
		self.overlay = overlay
	end

	overlay:SetVertexColor(getAuraCategoryColor(auraCategory))

	--[[ Text that is duration. ]]--

	local label1 = self.label1 or _G[string.format('%sText1', frameName)]
	assert(label1 ~= nil)
	if not self.label1 then
		self.label1 = label1
	end

	if ownerDesignation and UnitIsUnit('player', ownerDesignation) then
		label1:SetTextColor(1, 225 / 255, 0)
	else
		label1:SetTextColor(1, 1, 1)
	end

	label1:SetText(nil)

	self:Show()

	assert(expirationInstance ~= nil)
	assert('number' == type(expirationInstance))
	assert(expirationInstance >= 0)

	if 0 == expirationInstance then
		label1:SetText('∞')
		return
	end

	self:SetScript('OnUpdate', function(f)
		local now = GetTime()
		local durationRemainingSec = expirationInstance - now

		if durationRemainingSec > 0 then
			local t = formatDuration(durationRemainingSec)
			label1:SetText(t)
		else
			f:SetScript('OnUpdate', nil)
			f:Hide()
		end
	end)
end

local function acceptUnitAura(self, unitDesignation)
	assert(self ~= nil)

	assert(unitDesignation ~= nil)
	assert('string' == type(unitDesignation))
	assert(string.len(unitDesignation) >= 1)
	assert(string.len(unitDesignation) <= 256)

	local owner = SecureButton_GetUnit(self)
	assert(owner ~= nil)
	assert('string' == type(owner))
	assert(string.len(owner) >= 1)
	assert(string.len(owner) <= 256)

	if not UnitIsUnit(owner, unitDesignation) then
		return
	end

	--[[ Empty filter is permissible. ]]--
	local filter = self:GetAttribute('filter') or SecureButton_GetAttribute(self, 'filter')

	--[[ Spell rank is irrelevant. ]]--
	local rank = nil

	local spellName = self:GetAttribute('spell')
	if not spellName then
		self:Hide()
		return
	end


	local auraName = UnitAura(unitDesignation, spellName, rank, filter)
	if auraName then
		applyUnitAura(self, UnitAura(unitDesignation, spellName, rank, filter))
	else
		self:SetScript('OnUpdate', nil)
		self:Hide()
	end
end

local function eventProcessor(self, eventCategory, ...)
	assert(self ~= nil)

	if 'UNIT_AURA' == eventCategory then
		local unitDesignation = select(1, ...)
		acceptUnitAura(self, ...)
	elseif 'ADDON_LOADED' == eventCategory then
		local addonName = select(1, ...)
		if 'daybreak' == addonName then
			acceptUnitAura(self, 'player')
		end
	elseif 'PLAYER_FOCUS_CHANGED' == eventCategory then
		acceptUnitAura(self, 'focus')
	elseif 'PLAYER_TARGET_CHANGED' == eventCategory then
		acceptUnitAura(self, 'target')
	end
end

local function init(self)
	assert(self ~= nil)

	self:UnregisterEvent('PLAYER_LOGIN')

	self:RegisterEvent('ADDON_LOADED')
	self:RegisterEvent('PLAYER_FOCUS_CHANGED')
	self:RegisterEvent('PLAYER_TARGET_CHANGED')
	self:RegisterEvent('UNIT_AURA')
	self:SetScript('OnEvent', eventProcessor)
end

function DaybreakAuraTemplate.main(self)
	assert(self ~= nil)

	self:RegisterEvent('PLAYER_LOGIN')
	self:SetScript('OnEvent', init)
end
