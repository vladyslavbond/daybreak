DaybreakProfileTemplate = {}

local function check(profileFrame)
	assert(profileFrame ~= nil)

	local buildInterface = select(4, GetBuildInfo())
	assert(buildInterface ~= nil)
	assert('number' == type(buildInterface))

	local profileInterface = profileFrame:GetAttribute('interface') or SecureButton_GetAttribute(profileFrame, 'interface')
	assert(profileInterface ~= nil)
	assert('number' == type(profileInterface))

	--[[ TODO Add checks for class, spec and talent group. ]]--
	local _, userClassDesignation = UnitClass('player')
	userClassDesignation = string.upper(strtrim(userClassDesignation))

	local profileClassDesignation = profileFrame:GetAttribute('class') or 'NONE'
	profileClassDesignation = string.upper(strtrim(profileClassDesignation))

	return profileInterface == buildInterface and userClassDesignation == profileClassDesignation
end

local function auraFrameApplySpellName(auraFrame, spellName)
	assert(auraFrame ~= nil)

	assert(spellName ~= nil)
	assert('string' == type(spellName))

	auraFrame:SetAttribute('spell', spellName)
end

local function auraFrameApplyCell(auraFrame, column, row)
	assert(auraFrame ~= nil)

	column = math.min(math.max(0, math.floor(math.abs(column))), 12)
	row = math.min(math.max(0, math.floor(math.abs(row))), 12)

	local x = column * auraFrame:GetWidth()
	local y = row * auraFrame:GetHeight()
	auraFrame:ClearAllPoints()
	auraFrame:SetPoint('TOPLEFT', x, -y)
end

local function auraFrameApplyRecord(auraFrame, record)
	assert(auraFrame ~= nil)

	assert(record ~= nil)
	assert('table' == type(record))

	--[[ Limit what keys are inherited and mapped on purpose,
	so that users may not apply arbitrary changes to the properties. ]]--

	if record.spell then
		auraFrame:SetAttribute('spell', record.spell)
	end

	if record.filter then
		auraFrame:SetAttribute('filter', record.filter)
	end

	if record.unit then
		auraFrame:SetAttribute('unit', record.unit)
	end

	if record.cell then
		local cell = record.cell
		auraFrameApplyCell(auraFrame, cell[1], cell[2])
	end
end

local function auraFrameUnapply(auraFrame)
	assert(auraFrame ~= nil)

	auraFrame:SetAttribute('spell', nil)
end

local function auraFrameApply(auraFrame, eitherRecordOrSpellName)
	if 'string' == type(eitherRecordOrSpellName) then
		auraFrameApplySpellName(auraFrame, eitherRecordOrSpellName)
	elseif 'table' == type(eitherRecordOrSpellName) then
		auraFrameApplyRecord(auraFrame, eitherRecordOrSpellName)
	end
end

local function apply(profileFrame, conf)
	assert(profileFrame ~= nil)

	assert(conf ~= nil)
	assert('table' == type(conf))

	local t = {profileFrame:GetChildren()}
	local i = 0
	local j = 0

	--[[ Process the configuration entries with the most default fallbacks first. ]]--
	--[[ That way sorting is more predictable visually at runtime. ]]--

	i = 0
	while(i < #conf) do
		i = i + 1
		local record = conf[i]
		if record and 'string' == type(record) then
			j = j + 1
			local b = t[j]
			if not b then
				error('DaybreakProfileTemplate.lua:function apply: not enough buttons for the given configuration map')
				break
			end

			auraFrameApply(b, record)
		end
	end

	i = 0
	while(i < #conf) do
		i = i + 1
		local record = conf[i]
		if record and 'table' == type(record) then
			j = j + 1
			local b = t[j]
			if not b then
				error('DaybreakProfileTemplate.lua:function apply: not enough buttons for the given configuration map')
				break
			end

			auraFrameApply(b, record)
		end
	end

	while(j < #t) do
		j = j + 1
		local b = t[j]
		assert(b ~= nil)
		auraFrameUnapply(b)
	end
end

local function applyIfNecessary(profileFrame)
	assert(profileFrame ~= nil)

	local classDesignation = profileFrame:GetAttribute('class')
	assert(classDesignation ~= nil, profileFrame:GetName() .. ': empty \"class\" property')
	assert('string' == type(classDesignation))
	classDesignation = string.upper(strtrim(classDesignation))

	local className = string.upper(string.sub(classDesignation, 1, 1))
	className = className .. string.lower(string.sub(classDesignation, 2, string.len(classDesignation)))

	local interfaceNumber = profileFrame:GetAttribute('interface')
	assert(interfaceNumber ~= nil)
	assert('number' == type(interfaceNumber))
	interfaceNumber = math.abs(math.floor(interfaceNumber))

	local m = string.format('DaybreakProfile%s%dConf', className, interfaceNumber)
	local conf = _G[m]
	if conf then
		assert(conf ~= nil)
		assert('table' == type(conf))
		apply(profileFrame, conf)
		Daybreak.trace('apply', m)
	else
		Daybreak.trace('warning: could not find configuration \"' .. m .. '\"')
	end
end

local function toggle(profileFrame)
	if check(profileFrame) then
		profileFrame:Show()
	else
		profileFrame:Hide()
	end
end

local function eventProcessor(profileFrame, eventCategory)
	assert(profileFrame ~= nil)

	if 'ADDON_LOADED' == eventCategory then
		toggle(profileFrame)
	elseif 'PLAYER_LOGIN' == eventCategory then
		applyIfNecessary(profileFrame)
	end
end

local function init(profileFrame)
	assert(profileFrame ~= nil)

	local n = profileFrame:GetName()

	local buttonSize = 30
	local column = 0
	local row = 0
	local columnMax = 6
	local rowMax = 24

	local i = 0
	while(i < rowMax * columnMax) do
		i = i + 1
		local m = string.format('%sAuraFrame%02d', n, i)
		local b = _G[m] or CreateFrame('FRAME', m, profileFrame, 'DaybreakAuraTemplate')
		assert(b ~= nil)
		auraFrameApplyCell(b, column, row)

		local x = column * buttonSize
		local y = row * buttonSize
		column = column + 1
		if column >= columnMax then
			row = row + 1
			column = 0
		end
	end
end

function DaybreakProfileTemplate.main(profileFrame)
	assert(profileFrame ~= nil)

	init(profileFrame)

	profileFrame:RegisterEvent('ADDON_LOADED')
	profileFrame:RegisterEvent('PLAYER_LOGIN')
	profileFrame:SetScript('OnEvent', eventProcessor)
end
